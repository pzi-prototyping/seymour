Seymour
=======

An Inkscape extension that brings a Python Turtle Graphics Interpreter to the SVG page.

Seymour implements a simple subset of python's built-in turtle module (so moving code from turtle to seymour should be pretty painless).

Penup commands basically create an SVG path element. turtle.color sets the stroke style used when the penup occurs.

turtle.text adds a text element and places it on the path resulting from the last penup.

 
Usage
-----

* Paste your code in a text object and select it.
* Choose Effects > Python > Seymour... and then Press APPLY

Example code:
		
  t = turtle
  t.fd(100)
  t.rt(90)
  t.fd(50)
  t.exitonclick()



Installation
------------

Copy the 2 files:
* seymour.inx
* seymour.py

To an "Inkscape extensions" folder.

On Linux (and Mac?), you can (eventually create) and add the files to your home folder:

* $HOME/.config/inkscape/extensions

In Windows, you can add them to the share\extensions folder inside of the main Inkscape folder

* C:\Program Files\Inkscape\share\extensions


(Hopefully) Helpful Links
--------------------------

* http://wiki.inkscape.org/wiki/index.php/PythonEffectTutorial

