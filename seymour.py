#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Copyright 2013, Piet Zwart Institute, MA Media Design 
http://pzwart3.wdka.hro.nl/wiki/
Released under a GPL3 license, see LICENSE

#XSS2qwXBUYv%s+<>dqw6wmq2om#HH$wac=:;==|i|nYUWQQW$WQQQWmBQmWBQWWBWQWWWmBBWWmQQQQ
Z2qpXooo2i+=|aaZW1n2vYwn###ovi>=||=;:;::+=|||{?VTSUQWWBBW#ZWWmmWmWmmWmWmBmW#QWQQ
ZoSXXX2!==>{av1XTTTYX#WTY+..."!""!<%|=========|*nSou3vSZVZXA#WWmBBWWWmmBWWBmWWQQ
W#oX2ov%%nnZX1|++<loG?.=;<_%|_,._,..+*~=i>==|==+;:==||{SYH$QWWBWWBWmWWWWmWBWWWQQ
#ZXXo{vnuvnvI+=::vnS(:i|v%;::::=====|=|===|||==+===::=={uwm#mWWWmmWWBWWmWWWmBQQQ
U2XYo2Iov%|i++:.=i%X:=|+: :;:;==v%lvvvvvvlvs%avv|=====+|YWBWWWWWWWWmQWmWBWWBWQQQ
#XoooIi>|=~-:+ ;:=v(:|---:===|ivvnoX2XoXSoXo1aaunvvsi==:=)3WWWWWBBBWmWBWWWWmQWQQ
#XSZ1>)}+=|=;:}"|=|>=;. .;=||ivnoXXXXXZZZXqomX2ouXS2Xo;==;=lXWBWWWWWBWBWWWWmQWQQ
mX1%**=::`:.__;=-=>:::.:==+<svoXZXXZZZSqw#XXnwXXd&SqXXS>::+={S##WWWBWWBWWBWWWWQQ
Wnz||is,=;=._|::_=- -::==+|iIo2XXXXmwmZZZYodXmXXSo#Y1i|||:==|{IXZ$WQBWWBWWWmWQQQ
1vs>>=:=|==||=ia+`  .:==++vvvSoXXZX22Y1qu#XXXSoZX1l|+iivv>:=|={*mw#mWQmQWmWWWWQQ
|%iawu2!3e(v>"'      -=|>|ivvSeXoZXXSXXXZ#XXwXX1|<ivdXXXXo>:;;iII####WWBWWWWBQQQ
<|2YI>+={1o+ _  .-..._==<v}1ou1XwoZZZXZZZZXZXXonowmZZZXXX#X,:;=|liYXXX#mWBWBWQQQ
|uni>>|=ii3;;+,._- .==|%{iv1uSSX2XZXXZXXXo2XoS2Xm#ZZZ1+{i*SX; ==|i%*YoX###WmWWQQ
Iiv>|<;=||<-:.:=.;;=<iii{%1SooSS2li|ivooXn2ooXX#Z#X2|==-%IISo,--=i=|=InXZ##WmQQQ
|=iii>+||+=::=::=;==|||%vv1****+=+|aommmmXoXXXZZ#SXioZaw|vo#Xo   --==<||*A###WQQ
i|==||(=|=>-.-::-=;+||ii%|++++|vuoXZZ#Z#Z##ZZXXZooe+){suq##XXZo ...;;+<i%|?9$WWQ
l|||i=>|>|..:.:;::==|=i||+=|invoXSZZXXXoXnSnn32ZX1|IvvXXXXX#XZ#c .-:::=|3ns%>2AW
vi|i+|=<+:.:.:::;===|||>|ivno2X2***}|**I1ii|iinmXZwunoXX#XXXXXUZc :;:::<=%YoowzY
n}ii||>:;::.-::;:===|||i|lo2SS}|<%.-:=%"!+||ilnSX#Z##XXS22SXXZ#ZX;--:===i<=i3#Zo
os%|l+=:;==:-::::=;+|=|iivno2c|ioXaa%|i%Ii|ilvnSSXXXZZ#XmXXZ#U###m;:.::||i|l>i3$
1|}=>==;=:;:;.:.:;==||iiivno2%=+{*IvuXXoovnvvnooo2SXXZZZXZX#U###ZZc:-:;:||ovsvvv
1i>|==|==;.==;...::|+|i|Ilvnnvvno2XXXXX#XvnvononvooXXZZZ##Xu1XX##Zo;-.:.:+*1l3qv
1|==|++|;;:.::.:  :-+|||ivvvoonooooXZZZX1ooo21IIIvnXXXX#XZ#2Il?SXZX(.: ;::iwwIXw
o%i=<==>::::: .: ...:=||ilvnn222XSXXXSnoS2SSo|<vvv2S2XXXmm#`--==<Xov;.:..:|*SlBh
pvi>=|=:==.....- ..  -;=|ilvnnoSo2ooXSSSoSXZn>|oSXno2oXXZZ'  ..:.:{1=-:.:=||SXQW
X|li>||=;=:..... -     :+||lvnnn2o2SS2XXXXX2vii|!+---"!!+-   ._...<l=-..-::)Y1#B
Xvii||===;:.=:...       -=||ivvnno2oXXXXXXZ1|===. .      ..._/====i|;   .::=+nWW
mZs|v|+=|==::... .   .    :==Iivvnno2S2XXXX>==::..    -:=oe" :<vvi>:: .   ;||ivd
Qm2vi%i|%;;;:;.:        .  :=|<vv1vvo2XSXX1==:.  ._;=_:-"=...=vIni>:.   ..:=)2v|
WWZzi|<v||=+:==.;.. .     ..-:=||lIvvnno2n>=.. .:=|+'_:-;..;|invnv:. `   ..=%nIi
Qmwouc|vi<|+:<:: :. .   .     .-:=||iiInvI|:....;:..:====|ivnzi+|i;.   .:-=;=|i|
QW#XS}ii%%||=|=:..-_ -         ..-===+|ii||=:.:=i||||||ilIv}|` _i|:- ...;:==ivl=
Qmov%svvu%i==|=::   . :          ..:-;=+|===:;:=|ivIvvIl|=+;  =i==.  ..:==||{nc;
QWmmZm#2n>|><<>=:.  .::-             -.:-:-:-::==<i|ivlii|>= ==:..  :==::=%o|*1>
QQ#mmWTon=Ivil==:.    =                . .-..- - :-++<|>|<=;:;:       -=<|Inoai_
QQQQQeXeniIi|I==:. .   : .                  -.  . : :-::.-.:.- .      .;+lXXQ#mB
QQQQQQWscs=|(>===:  :.::..   .                 ..... -.. ...      .  ====iinodWm
QQQQQQk2oi|+i>><:.::=..<;_:.. .                -  -  ..;:  . .    ..::+i|vwumQmW
QQQQQBdW#Xviii|=|<=_.:||%%===;;..    .        -...:-, .:::...:   :=;==|<vnmmQWWT
"""

import sys, os
if os.path.exists('/usr/share/inkscape/extensions'):
    sys.path.append('/usr/share/inkscape/extensions') # or another path, as necessary
from simplestyle import *
import random, math, uuid
import inkex

defaultpathstyles = {
    'fill':'none',
    'fill-rule':'evenodd',
    'stroke':'#000000',
    'stroke-width':'1px',
    'stroke-linecap':'butt',
    'stroke-linejoin':'miter',
    'stroke-opacity':'1'
}

#################################

NBSP = "\xc2\xa0"

def zap_nbsp (bytes):
    return bytes.replace(NBSP, " ")

def colorval (f):
    if (type(f) == float):
        if f>= 0 and f<1:
            f = math.floor(256*f)
        else:
            f = math.floor(f)
    return f
def rgb (r, g, b):
    return '#%02x%02x%02x' % (colorval(r),colorval(g),colorval(b))

# <path style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="%s" />
def pointstodata (points):
    try:
        data = "M %0.6f,%0.6f" % points[0]
        for p in points[1:]:
            data += " L %0.6f,%0.6f" % p
        return data
    except IndexError:
        return ""

def collecttext (elt):
    ret = elt.text or ""
    ret += "\n".join([collecttext(child) for child in elt])
    return ret

#################################

class Turtle (object):
    def __init__(self, x=0.0, y=0.0, heading=0.0, pendown=True, layer=None):
        self.x = x
        self.y = y
        self.homex = x
        self.homey = y
        self.heading = heading
        self._pendown = False
        self.points = []
        self.stack = []
        self.lastpath = None
        if layer is not None:
            self.stack.append(layer)
        self.styles = {}
        if pendown:
            self.pendown()

    def forward (self, steps):
        self.x += math.cos(self.heading) * steps
        self.y -= math.sin(self.heading) * steps
        if self._pendown:
            self.points.append((self.x, self.y))
    fd = forward

    def backward (self, steps):
        self.forward(-steps)
    bk = backward
    back = backward

    def right (self, d):
        d = (d/180.0)*math.pi
        self.heading -= d
    rt = right

    def left (self, d):
        d = (d/180.0)*math.pi
        self.heading += d
    lt = left

    def goto(self, x, y):
        self.x = x
        self.y = y
    setpos = goto
    setposition = goto

    def setx (self, x):
        self.x = x

    def sety (self, y):
        self.y = y

    def setheading(self, d):
        self.heading = (d/180.0)*math.pi
    seth = setheading
    face = setheading

    def home(self):
        self.x = homex
        self.y = homey

    def circle(self, radius, extent=None, steps=None):
        pass
    def dot(self, size=None, *color):
        pass
    def stamp(self):
        pass
    def clearstamp(self, stampid):
        pass
    def clearstamps(self, n=None):
        pass
    def undo(self):
        pass
    def speed(self, speed=None):
        pass

    def pendown (self):
        if not self._pendown:
            self._pendown = True
            self.points.append((self.x, self.y))
    pd = pendown
    
    def penup (self):
        if self._pendown:
            self._pendown = False
            if len(self.points) and len(self.stack):
                path = inkex.etree.Element(inkex.addNS('path', 'svg'))
                # http://stackoverflow.com/questions/1551666/how-can-2-python-dictionaries-become-1/1551878#1551878
                styles =  dict(defaultpathstyles, **self.styles)
                path.set('style', formatStyle(styles))
                path.set('d', pointstodata(self.points))
                self.stack[-1].append(path)
                self.lastpath = path
                self.points = []
    pu = penup

    def ensure_id (self, elt):
        try:
            ret = elt.attrib['id']
        except KeyError:
            ret = "seymour-"+uuid.uuid1().hex
            elt.attrib['id'] = ret

        return ret

    def text (self, text):
        if self.lastpath != None:
            textelt = inkex.etree.Element(inkex.addNS('text', 'svg'))
            textPath = inkex.etree.Element(inkex.addNS('textPath', 'svg'))
            # assert False, str(self.lastpath.attrib.keys())
            textPath.attrib[inkex.addNS('href', 'xlink')] = '#'+self.ensure_id(self.lastpath)
            textPath.text = text
            textelt.append(textPath)
            self.stack[-1].append(textelt)
        else:
            textelt = inkex.etree.Element(inkex.addNS('text', 'svg'))
            textelt.text = text
            self.stack[-1].append(textelt)

    def color (self, *args):
        if len(args) == 3:
            r, g, b = args
            self.styles['stroke'] = rgb(r, g, b)

        elif len(args) == 1:
            r, g, b = args[0]
            self.styles['stroke'] = rgb(r, g, b)


    def startgroup (self, id=None):
        """ special to SVG"""
        g = inkex.etree.Element(inkex.addNS('g', 'svg'))
        if id is not None:
            g.set('id', str(id))
        self.stack[-1].append(g)
        self.stack.append(g)

    def endgroup (self):
        """ special to SVG"""
        self.stack.pop()

    def exitonclick (self):
        self.penup()
        pass


#########################################################
# The Inkscape Effect interface

class SeymourEffect(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        # http://docs.python.org/lib/module-optparse.html
        self.OptionParser.add_option('-l', '--layer', action = 'store',
          type = 'string', dest = 'layer', default = 'Seymour output',
          help = 'name of layer for output')

    def effect (self):
        svg = self.document.getroot()
        # svg = self.document.xpath('//svg:svg', namespace = inkex.NSS)[0]
        for id, code in self.selected.iteritems():
            try:
                # code = self.document.xpath("//*[@id='%s']" % codeid)[0]
                src = collecttext(code)
                src = zap_nbsp(src.encode("utf-8"))
                width = self.unittouu(svg.get('width'))
                height = self.unittouu(svg.attrib['height'])
                outputlayer = inkex.etree.SubElement(svg, 'g')
                outputlayer.set(inkex.addNS('label', 'inkscape'), self.options.layer)
                outputlayer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
                
                turtle = Turtle(x=width/2, y=height/2, layer=outputlayer)
                d = {'turtle': turtle}
                # turtle.export_to_dict(d)
                # d['rgb'] = rgb
                exec src in d
            except IndexError:
                pass

##############################################################################
# This file can be directly applied to an SVG on the commandline using:
# python seymour.py path/to/your.svg

if __name__ == "__main__":
    effect = SeymourEffect()
    effect.affect()    

